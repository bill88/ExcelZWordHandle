# Python Excel数据处理自动化生成Word文档（含索引目录 分页）

#### Description
通过Python 批量处理遍历文件目录中的Excel文件，对Excel数据进行筛选、排序、合并等处理并获取到结果数据，存储到DataFrame中；将DataFrame中的数据通过Python docx 写入到提前读取的Word模板中，自动化生成Word字典文件（含索引目录及分页）
流程：遍历目录->获取Excel list->数据筛选、合并->读取Word模板->数据写入Word（生成文档首页、目录标题、正文、表格、样式处理等）->更新索引目录->生成结果Word文件

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
